package core;

import lombok.Data;
import lombok.Getter;

@Data
public class Parameters {

    private final static int ARGUMENTS_NUMBER = 2;
    @Getter
    private String token = "t.2cXPcfhY3X1a5BEyuAhtaqlmfiIGygMBnimkH9bn05Rq9SWVU_SaGK3SYXRPJ97BI9HoL2b7ysFy-Fq8Og1nHQ";
    private boolean sandBoxMode = true;

    public Parameters(String[] args) {
        if (args.length < 2)
            throw new IllegalArgumentException(String.format(
                    "Invalid number of arguments [%d], expected [%d]",
                    args.length, ARGUMENTS_NUMBER));
        setParameters("t.2cXPcfhY3X1a5BEyuAhtaqlmfiIGygMBnimkH9bn05Rq9SWVU_SaGK3SYXRPJ97BI9HoL2b7ysFy-Fq8Og1nHQ", true);
    }

    public Parameters(String token, boolean sandBoxMode) {
        setParameters(token, sandBoxMode);
    }

    private void setParameters(String token, boolean sandBoxMode) {
        this.token = token;
        this.sandBoxMode = sandBoxMode;
    }

    @Override
    public final String toString() {
        return String.format("core.Parameters: sandBoxMode = %s", sandBoxMode ? "true" : "false");
    }

}